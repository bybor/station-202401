using System;
using System.Linq;
using Common;

namespace PathFinder
{
    internal class Program
    {

        private static void Main(string[] _) {
            Station station = StationRepository.Instance.GetStationByName(StationRepository.STATION_PATH_FINDER);

            if (station == null) {
                throw new Exception($"StationRepository doesn't have a station with name '{StationRepository.STATION_PATH_FINDER}");
            }

            Console.WriteLine("PRESS CTRL-C TO EXIT when done");
            Console.WriteLine();

            Segment[] segments = station.GetSegments();


            while (true) {
                Console.WriteLine();
                int index = 0;
                foreach (var s in segments) {
                    Console.Write($"Index: {index}. Segment: {s}. Label: {s.NumberLabel}.");
                    if (s.Name != s.NumberLabel)
                        Console.Write($" Name: {s.Name}");
                    Console.WriteLine();
                    index++;
                }
                Console.WriteLine();

                int indexFrom;
                try {
                    indexFrom = InputNumber("Type index of 'from' segment and press Enter: ", 0, segments.Length - 1);
                }
                catch (Exception e) {
                    Console.WriteLine(e.Message);
                    continue;
                }

                int indexTo;
                try {
                    indexTo = InputNumber("Type index of 'to' segment and press Enter: ", 0, segments.Length - 1);
                }
                catch (Exception e) {
                    Console.WriteLine(e.Message);
                    continue;
                }

                IShortestPathFinder solver = new DijkstraShortestPathSolver();
                Point[] points = solver.Find(segments, segments[indexFrom], segments[indexTo]);
                if (points.Length == 0) {
                    Console.WriteLine("NO PATH");
                }
                else {
                    Console.WriteLine("PATH: {0}", string.Join("; ", points.Select(p => p.ToString())));
                }
            }
        }

        private static int InputNumber(string prompt, int min, int max) {
            Console.Write(prompt);

            string input = Console.ReadLine();

            if (!int.TryParse(input, out int value)) {
                throw new Exception($"Error. Cannot parse '{input}' as integer");
            }
            else if (value < min || value > max) {
                throw new Exception($"Error. Incorrect index value {value}");
            }

            return value;
        }
    }
}
