using System;
using System.Linq;
using Common;

namespace ParkFill
{
    internal class Program
    {
        private static void Main(string[] _) {
            Station s = StationRepository.Instance.GetStationByName(StationRepository.STATION_PARK_FILL);

            if (s == null) {
                throw new Exception($"StationRepository doesn't have a station with name '{StationRepository.STATION_PARK_FILL}");
            }

            foreach ((string ParkName, Way[] Ways, Point[] Vertices) park in s.GetParks()) {
                Console.WriteLine($"Park: {park.ParkName}");
                Console.WriteLine(string.Join("; ", park.Vertices.Select(t => t.ToString())));
                Console.WriteLine();
            }
        }
    }
}
