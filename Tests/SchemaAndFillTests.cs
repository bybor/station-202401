using System.Collections.Generic;
using System.Linq;
using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class SchemaAndFillTests
    {
        [TestMethod]
        public void TestMethod1() {
            Station testStation = new Station("test1", new Segment[]
            {
                new Segment(new Point(2, 2), new Point(2, 7), name: "A"),
            });

            testStation.AddWay("testWay", new string[] { "a" });
            testStation.AddPark("p1", new string[] { "testWay" });

            var r = testStation.GetParks();

            Assert.IsTrue(r.Length == 1);

            (string ParkName, Way[] Ways, Point[] Vertices) testPark = r[0];

            Assert.AreEqual("p1", testPark.ParkName);
            Assert.AreEqual(1, testPark.Ways.Length);

            Assert.AreEqual(2, testPark.Vertices.Length);

            Assert.IsTrue(testPark.Vertices.Contains(new Point(2, 2)));
            Assert.IsTrue(testPark.Vertices.Contains(new Point(2, 7)));
        }

        [TestMethod]
        public void TestMethod2() {
            Station testStation = new Station("test2", new Segment[]
            {
                new Segment(new Point(2, 2), new Point(2, 7), name: "a"),
                new Segment(new Point(5, 1), new Point(5, 8), name: "b"),
            });

            testStation.AddWay("testWay", new string[] { "a", "b" });
            testStation.AddPark("p1", new string[] { "testWay" });

            var r = testStation.GetParks();

            (string ParkName, Way[] Ways, Point[] Vertices) testPark = r[0];

            Assert.AreEqual("p1", testPark.ParkName);
            Assert.AreEqual(1, testPark.Ways.Length);

            Assert.AreEqual(4, testPark.Vertices.Length);

            Assert.IsTrue(testPark.Vertices.Contains(new Point(2, 2)));
            Assert.IsTrue(testPark.Vertices.Contains(new Point(2, 7)));
            Assert.IsTrue(testPark.Vertices.Contains(new Point(5, 1)));
            Assert.IsTrue(testPark.Vertices.Contains(new Point(5, 8)));
        }

        [TestMethod]
        public void TestMethod3() {
            Station testStation = new Station("test3", new Segment[]
            {
                new Segment(new Point(3, 3), new Point(10, 10), name: "a"),
                new Segment(new Point(10, 10), new Point(20, 20), name: "b"),
                new Segment(new Point(1, 30), new Point(10, 10), name: "c"),
                new Segment(new Point(10, 10), new Point(20, 1), name: "d"),
            });

            testStation.AddWay("P1", new string[] { "a", "b" });
            testStation.AddWay("P2", new string[] { "c", "d" });
            testStation.AddPark("PARK", new string[] { "P1", "P2" });

            var r = testStation.GetParks();
            Assert.AreEqual(1, r.Length);

            (string ParkName, Way[] Ways, Point[] Vertices) testPark = r[0];

            Assert.AreEqual("PARK", testPark.ParkName);
            Assert.AreEqual(2, testPark.Ways.Length);

            Assert.AreEqual(4, testPark.Vertices.Length);

            Assert.IsTrue(testPark.Vertices.Contains(new Point(3, 3)));
            Assert.IsTrue(testPark.Vertices.Contains(new Point(20, 20)));
            Assert.IsTrue(testPark.Vertices.Contains(new Point(1, 30)));
            Assert.IsTrue(testPark.Vertices.Contains(new Point(20, 1)));
        }

        [TestMethod]
        public void TestMethod4() {
            Station testStation = new Station("test4", new Segment[]
            {
                new Segment(new Point(8, 3), new Point(8, 5)),
                new Segment(new Point(8, 5), new Point(8, 10)),
                new Segment(new Point(8, 10), new Point(8, 18)),
                new Segment(new Point(8, 18), new Point(8, 21)),
                new Segment(new Point(8, 21), new Point(8, 23)),
            });

            testStation.AddWay("P Line", new[]
            {
                "008.003-008.005",
                "008.005-008.010",
                "008.010-008.018",
                "008.018-008.021",
                "008.021-008.023",
            });

            testStation.AddPark("Park Line", new string[] { "P Line" });

            var r = testStation.GetParks();

            (string ParkName, Way[] Ways, Point[] Vertices) testPark = r[0];

            Assert.AreEqual(2, testPark.Vertices.Length);

            Assert.IsTrue(testPark.Vertices.Contains(new Point(8, 3)));
            Assert.IsTrue(testPark.Vertices.Contains(new Point(8, 23)));
        }

    }
}
