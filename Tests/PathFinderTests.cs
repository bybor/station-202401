using System.Collections.Generic;
using System.Linq;
using Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class PathFinderTests
    {
        [TestMethod]
        public void TestMethod1() {
            Station station = new(
                stationName: "Copy",
                segments: new[]
                {
                    new Segment(new Point(4, 0), new Point(4, 12)),
                    new Segment(new Point(4, 0), new Point(7, 0)),
                    new Segment(new Point(4, 0), new Point(6, 2)),
                    new Segment(new Point(7, 0), new Point(6, 2)),
                    new Segment(new Point(6, 2), new Point(7, 9)),
                    new Segment(new Point(7, 9), new Point(4, 12)),
                    new Segment(new Point(7, 0), new Point(9, 2)),
                    new Segment(new Point(9, 2), new Point(7, 9)),
                    new Segment(new Point(2, 7), new Point(2, 10)), // not connected to anything else
                });

            Segment from = station.GetSegmentByName("004.000-004.012");
            Assert.IsNotNull(from);
            Segment to = station.GetSegmentByName("009.002-007.009");
            Assert.IsNotNull(to);

            IShortestPathFinder alg = new DijkstraShortestPathSolver();
            Point[] path = alg.Find(station.GetSegments(), from, to);
            Assert.AreEqual(3, path.Length);
            Assert.AreEqual(path[0], new Point(4, 0));
            Assert.AreEqual(path[1], new Point(7, 0));
            Assert.AreEqual(path[2], new Point(9, 2));
        }

        [TestMethod]
        public void TestMethod2() {
            Station station = new(
                stationName: "Copy",
                segments: new[]
                {
                    new Segment(new Point(4, 0), new Point(4, 12)),
                    new Segment(new Point(4, 0), new Point(7, 0)),
                    new Segment(new Point(4, 0), new Point(6, 2)),
                    new Segment(new Point(7, 0), new Point(6, 2)),
                    new Segment(new Point(6, 2), new Point(7, 9)),
                    new Segment(new Point(7, 9), new Point(4, 12)),
                    new Segment(new Point(7, 0), new Point(9, 2)),
                    new Segment(new Point(9, 2), new Point(7, 9)),
                    new Segment(new Point(2, 7), new Point(2, 10)), // not connected to anything else
                });

            Segment from = station.GetSegmentByName("004.000-004.012");
            Assert.IsNotNull(from);
            Segment to = station.GetSegmentByName("007.009-004.012");
            Assert.IsNotNull(to);

            IShortestPathFinder alg = new DijkstraShortestPathSolver();
            Point[] path = alg.Find(station.GetSegments(), from, to);
            Assert.AreEqual(3, path.Length);
            Assert.AreEqual(path[0], new Point(4, 0));
            Assert.AreEqual(path[1], new Point(6, 2));
            Assert.AreEqual(path[2], new Point(7, 9));
        }

        [TestMethod]
        public void TestMethod3() {
            Station station = new(
                stationName: "Copy",
                segments: new[]
                {
                    new Segment(new Point(4, 0), new Point(4, 12)),
                    new Segment(new Point(4, 0), new Point(7, 0)),
                    new Segment(new Point(4, 0), new Point(6, 2)),
                    new Segment(new Point(7, 0), new Point(6, 2)),
                    new Segment(new Point(6, 2), new Point(7, 9)),
                    new Segment(new Point(7, 9), new Point(4, 12)),
                    new Segment(new Point(7, 0), new Point(9, 2)),
                    new Segment(new Point(9, 2), new Point(7, 9)),
                    new Segment(new Point(2, 7), new Point(2, 10)), // not connected to anything else
                });

            Segment from = station.GetSegmentByName("009.002-007.009");
            Assert.IsNotNull(from);
            Segment to = station.GetSegmentByName("002.007-002.010");
            Assert.IsNotNull(to);

            IShortestPathFinder alg = new DijkstraShortestPathSolver();
            Point[] path = alg.Find(station.GetSegments(), from, to);
            Assert.AreEqual(0, path.Length);
        }

    }
}
