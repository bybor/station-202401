using System;
using System.Diagnostics;

namespace Common
{
    [DebuggerDisplay("({X}, {Y})")]
    public class Point : IEquatable<Point>
    {
        public int X { get; }

        public int Y { get; }

        public Point(int x, int y) {
            X = x;
            Y = y;
        }

        public override bool Equals(object obj) {
            if (obj != null && obj is Point p) {
                return p.X == X && p.Y == Y;
            }

            return false;
        }

        public override int GetHashCode() {
            return X ^ Y;
        }

        public bool Equals(Point other) {
            if (other == null) {
                return false;
            }
            else {
                return other.X == X && other.Y == Y;
            }
        }

        public override string ToString() {
            return $"({X}, {Y})";
        }
    }

}
