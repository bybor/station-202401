using System;

namespace Common
{
    public class Segment
    {
        public Segment(Point begin, Point end, string name = null) {
            if (begin == null) {
                throw new ArgumentNullException(nameof(begin));
            }

            if (end == null) {
                throw new ArgumentNullException(nameof(end));
            }

            if (begin == end) {
                throw new ArgumentException("Segment's begin and end must be at different points");
            }

            // Make sure that we define segments as being "left to right" and "top to down"

            if (begin.Y < end.Y) {
                Begin = begin;
                End = end;
            }
            else if (begin.Y > end.Y) {
                Begin = end;
                End = begin;
            }
            else if (begin.X < end.X) // at this point we know that both points have the same Y value
            {
                Begin = begin;
                End = end;
            }
            else {
                Begin = end;
                End = begin;
            }


            if (name == null) {
                Name = NumberLabel;
            }
            else {
                Name = name;
            }
        }

        public Point Begin { get; }
        public Point End { get; }

        public string NumberLabel => $"{Begin.X:D3}.{Begin.Y:D3}-{End.X:D3}.{End.Y:D3}";

        public string Name { get; }

        public override bool Equals(object obj) {
            if (obj != null && obj is Segment s) {
                return s.Begin == Begin && s.End == End;
            }

            return false;
        }

        public override int GetHashCode() {
            return NumberLabel.GetHashCode();
        }

        public override string ToString() {
            return $"{Begin}-{End}";
        }

    }

}
