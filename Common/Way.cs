using System.Collections.Generic;

namespace Common
{
    public class Way
    {
        private readonly List<Segment> segments;

        public Way(string name, IEnumerable<Segment> segments) {
            Name = name;
            this.segments = new List<Segment>(segments);
        }

        public string Name { get; }

        public Segment[] GetSegments() {
            return segments.ToArray();
        }
    }

}
