using System;
using System.Collections.Generic;
using System.Linq;

namespace Common
{
    public class Station
    {
        private readonly SortedDictionary<string, Segment> segmentsByName = new(StringComparer.OrdinalIgnoreCase);

        public string Name { get; }

        public Station(string stationName, Segment[] segments) {
            Name = stationName;

            foreach (Segment s in segments) {
                if (segmentsByName.ContainsKey(s.Name)) {
                    throw new Exception($"Duplicate segment name '{s.Name}'");
                }

                segmentsByName.Add(s.Name, s);
            }
        }

        public Segment[] GetSegments() {
            return segmentsByName.Values.ToArray();
        }

        public Segment GetSegmentByName(string name) {
            if (segmentsByName.TryGetValue(name, out Segment segment)) {
                return segment;
            }
            else {
                return null;
            }
        }

        private readonly Dictionary<string, Way> stationWaysByName = new(StringComparer.OrdinalIgnoreCase);

        public void AddWay(string wayName, string[] segmentNames) {
            if (stationWaysByName.ContainsKey(wayName)) {
                throw new Exception($"Station already has a Way with a name: '{wayName}'");
            }

            var list = new List<Segment>();

            foreach (string name in segmentNames) {
                if (!segmentsByName.TryGetValue(name, out Segment segment)) {
                    throw new Exception($"Segment with name '{name}' doesn't belong to station");
                }
                else {
                    list.Add(segment);
                }
            }

            var w = new Way(wayName, list);
            stationWaysByName.Add(wayName, w);

        }

        private readonly Dictionary<string, Park> parksByName = new(StringComparer.OrdinalIgnoreCase);

        public void AddPark(string parkName, string[] wayNames) {
            if (string.IsNullOrWhiteSpace(parkName)) {
                throw new ArgumentNullException(nameof(parkName));
            }

            if (parksByName.ContainsKey(parkName)) {
                throw new Exception($"Station already has a park with a name '{parkName}'");
            }

            var list = new List<Way>();

            foreach (string name in wayNames) {
                if (!stationWaysByName.TryGetValue(name, out Way w)) {
                    throw new Exception($"Way with name '{name}' doesn't belong to station");
                }
                else {
                    list.Add(w);
                }
            }

            parksByName.Add(parkName, new Park(parkName, list));
        }

        public (string ParkName, Way[] Ways, Point[] Vertices)[] GetParks() {
            return parksByName.Select(kvp => (kvp.Key, kvp.Value.GetWays(), kvp.Value.GetFill())).ToArray();
        }
    }

}
