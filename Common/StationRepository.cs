using System;
using System.Collections.Generic;
using System.Linq;

namespace Common
{
    public interface IStationRepository
    {
        Station[] GetStations();

        Station GetStationByName(string name);
    }

    public class StationRepository : IStationRepository
    {
        public const string STATION_PARK_FILL = "ParkFill";
        public const string STATION_PATH_FINDER = "PathFinder";

        public static IStationRepository Instance { get; } = new StationRepository();

        private static readonly Dictionary<string, Station> Stations = new(StringComparer.OrdinalIgnoreCase);

        static StationRepository() {
            Station s = CreateParkFillStation();
            Stations.Add(s.Name, s);

            s = CreatePathFinderStation();
            Stations.Add(s.Name, s);
        }

        public Station GetStationByName(string name) {
            if (!Stations.TryGetValue(name, out Station s)) {
                throw new Exception($"No station with name '{name}'");
            }
            else {
                return s;
            }
        }

        public Station[] GetStations() {
            return Stations.Values.ToArray();
        }

        private static Station CreatePathFinderStation() {
            Station s = new(
                stationName: STATION_PATH_FINDER,
                segments: new[]
                {
                    new Segment(new Point(4, 0), new Point(4, 12)),
                    new Segment(new Point(4, 0), new Point(7, 0)),
                    new Segment(new Point(4, 0), new Point(6, 2)),
                    new Segment(new Point(7, 0), new Point(6, 2)),
                    new Segment(new Point(6, 2), new Point(7, 9)),
                    new Segment(new Point(7, 9), new Point(4, 12)),
                    new Segment(new Point(7, 0), new Point(9, 2)),
                    new Segment(new Point(9, 2), new Point(7, 9)),
                    new Segment(new Point(12, 7), new Point(12, 10), name: "NOT CONNECTED"), // not connected to anything else
                });
            ;

            return s;
        }

        private static Station CreateParkFillStation() {
            Station s = new(
                stationName: STATION_PARK_FILL,
                segments: new[]
                {
                    new Segment(new Point(2, 10), new Point(2, 21)),

                    new Segment(new Point(4, 3), new Point(4, 7)),
                    new Segment(new Point(4, 7), new Point(4, 8)),
                    new Segment(new Point(4, 8), new Point(2, 10)),
                    new Segment(new Point(4, 11), new Point(4, 18)),
                    new Segment(new Point(4, 18), new Point(4, 23)),

                    new Segment(new Point(6, 0), new Point(6, 5)),
                    new Segment(new Point(6, 5), new Point(4, 7)),
                    new Segment(new Point(6, 5), new Point(6, 16)),
                    new Segment(new Point(6, 16), new Point(4, 18)),

                    new Segment(new Point(8, 3), new Point(8, 5)),
                    new Segment(new Point(8, 5), new Point(8, 10)),
                    new Segment(new Point(8, 10), new Point(8, 18)),
                    new Segment(new Point(8, 18), new Point(8, 21)),
                    new Segment(new Point(8, 21), new Point(8, 23)),

                    new Segment(new Point(10, 0), new Point(10, 3)),
                    new Segment(new Point(10, 0), new Point(12, 2)),
                    new Segment(new Point(10, 3), new Point(8, 5)),
                    new Segment(new Point(10, 3), new Point(10, 5)),
                    new Segment(new Point(10, 5), new Point(12, 7)),
                    new Segment(new Point(10, 16), new Point(10, 19)),
                    new Segment(new Point(10, 19), new Point(8, 21)),
                    new Segment(new Point(10, 19), new Point(10, 21)),

                    new Segment(new Point(12, 2), new Point(14, 4)),
                    new Segment(new Point(12, 7), new Point(12, 10)),
                    new Segment(new Point(12, 10), new Point(12, 14)),
                    new Segment(new Point(12, 10), new Point(14, 12)),
                    new Segment(new Point(12, 14), new Point(10, 16)),

                    new Segment(new Point(14, 0), new Point(14, 4)),
                    new Segment(new Point(14, 4), new Point(14, 5)),
                    new Segment(new Point(14, 5), new Point(16, 7)),
                    new Segment(new Point(14, 12), new Point(16, 14)),
                    new Segment(new Point(14, 12), new Point(14, 19)),


                    new Segment(new Point(16, 7), new Point(16, 10)),
                    new Segment(new Point(16, 10), new Point(16, 14)),
                    new Segment(new Point(16, 14), new Point(16, 17)),
                    new Segment(new Point(16, 17), new Point(18, 19)),


                    new Segment(new Point(18, 2), new Point(18, 8)),
                    new Segment(new Point(18, 8), new Point(16, 10)),
                    new Segment(new Point(18, 8), new Point(18, 15)),
                    new Segment(new Point(18, 15), new Point(20, 17)),
                    new Segment(new Point(18, 19), new Point(18, 21)),
                }
            );
            s.AddWay("P1", new[]
            {
                "004.003-004.007",
                "004.007-004.008",
                "004.008-002.010",
                "002.010-002.021",

            });
            s.AddWay("P2", new[]
            {
                "010.000-010.003",
                "010.003-010.005",
                "010.005-012.007",
                "012.007-012.010",
                "012.010-014.012",
                "014.012-014.019",
            });
            s.AddWay("P3", new[]
            {
                "014.012-014.019",
            });

            s.AddPark("Park1", new string[] { "P1", "P2" });

            s.AddWay("P Line", new[]
            {
                "008.003-008.005",
                "008.005-008.010",
                "008.010-008.018",
                "008.018-008.021",
                "008.021-008.023",
            });
            s.AddPark("Park Line", new string[] { "P Line" });


            s.AddWay("P4", new[]
            {
                "018.002-018.008",
                "018.008-016.010",
                "016.010-016.014",
                "016.014-016.017",
                "016.017-018.019",
            });
            s.AddPark("Park7", new string[] { "P4", "P3" });

            return s;
        }
    }

}
