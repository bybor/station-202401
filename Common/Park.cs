using System.Collections.Generic;
using System.Linq;

namespace Common
{
    public class Park
    {
        private readonly List<Way> ways;

        public Park(string parkName, IEnumerable<Way> ways) {
            Name = parkName;
            this.ways = new List<Way>(ways);
        }

        public string Name { get; }

        private Segment[] GetSegments() {
            return ways.SelectMany(w => w.GetSegments()).ToArray();
        }

        public Way[] GetWays() {
            return ways.ToArray();
        }

        public Point[] GetFill() {
            var segments = new List<Segment>(GetSegments());

            var list = new List<Point>();

            foreach (Segment s in segments) {
                var points = new Point[] { s.Begin, s.End };

                foreach (var p in points) {
                    list.Add(p);
                }
            }

            IFillFinder fillFinder = new JarvisConvexHull();
            var result = fillFinder.Solve(list.ToArray());
            return result;
        }
    }

}
