using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public interface IFillFinder
    {
        Point[] Solve(Point[] points);
    }

    public class JarvisConvexHull : IFillFinder
    {
        public Point[] Solve(Point[] points) {
            var result = new List<Point>();
            var source = new HashSet<Point>();

            Point p0 = new(int.MaxValue, int.MaxValue);

            foreach (Point p in points) {
                source.Add(p);

                if (p.X < p0.X || (p.X == p0.X && p.Y < p0.Y)) {
                    p0 = p;
                }
            }

            if (source.Count < 3) {
                return source.ToArray();
            }
            else {
                points = source.ToArray();

                Point t;

                do {
                    result.Add(p0);
                    t = points[0];

                    for (int i = 1; i < points.Length; i++) {
                        int orientation = Orientation(p0, t, points[i]);
                        if (p0 == t || orientation == -1
                            || (orientation == 0 && QuadraticDistance(points[i], p0) > QuadraticDistance(t, p0))) {
                            t = points[i];
                        }
                    }

                    p0 = t;

                } while (t != result[0]);

                return result.ToArray();
            }
        }

        private static int QuadraticDistance(Point p1, Point p2) {
            return (p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y);
        }

        private static int Orientation(Point p1, Point p2, Point p) {
            int o = (p2.X - p1.X) * (p.Y - p1.Y) - (p.X - p1.X) * (p2.Y - p1.Y);
            if (o > 0) {
                return -1;
            }
            else if (o < 0) {
                return 1;
            }
            else {
                return 0;
            }
        }

    }
}
