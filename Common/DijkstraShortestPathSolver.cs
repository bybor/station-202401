using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Common
{
    public interface IShortestPathFinder
    {
        Point[] Find(Segment[] segments, Segment begin, Segment end);
        int[] Find(int[,] adjacencyMatrix, int sourceIndex, int targetIndex);
    }

    public class DijkstraShortestPathSolver : IShortestPathFinder
    {
        // We need this because .NET 5 doesn't have `PriorityQueue` class and `SortedList` doesn't allow duplicate keys
        public class DuplicateKeyComparer<TKey> : IComparer<TKey>
            where TKey : IComparable
        {
            public int Compare(TKey x, TKey y) {
                int result = x.CompareTo(y);

                if (result == 0) {
                    return -1;
                }
                else {
                    return result;
                }
            }
        }

        public Point[] Find(Segment[] segments, Segment start, Segment finish) {
            var indexOfPoint = new Dictionary<Point, int>();
            var pointByIndex = new Dictionary<int, Point>();

            int indexValue = 0;
            foreach (Segment s in segments) {
                foreach (Point p in new Point[] { s.Begin, s.End }) {
                    if (!indexOfPoint.TryGetValue(p, out int _)) {
                        indexOfPoint.Add(p, indexValue);
                        pointByIndex.Add(indexValue, p);

                        indexValue++;
                    }
                }
            }

            int[,] adjacencyMatrix = new int[indexValue, indexValue];

            foreach (Segment s in segments) {
                int indexBegin = indexOfPoint[s.Begin];
                int indexEnd = indexOfPoint[s.End];

                int distance = QuadraticDistance(s.Begin, s.End);
                adjacencyMatrix[indexBegin, indexEnd] = distance;
                adjacencyMatrix[indexEnd, indexBegin] = distance;
            }

            if (!indexOfPoint.TryGetValue(start.Begin, out int sourceIndex)) {
                throw new Exception($"Input set of segments doesn't include a starting segment '{start.NumberLabel}'");
            }

            if (!indexOfPoint.TryGetValue(finish.Begin, out int targetIndex)) {
                throw new Exception($"Input set of segments doesn't include a finishing segment '{finish.NumberLabel}'");
            }

            int[] found = Find(adjacencyMatrix, sourceIndex, targetIndex);
            if (found.Length == 0) {
                return Array.Empty<Point>();
            }
            else {
                var list = new List<Point>();

                foreach (int i in found) {
                    list.Add(pointByIndex[i]);
                }

                return list.ToArray();
            }
        }

        private static int QuadraticDistance(Point p1, Point p2) {
            return (p2.X - p1.X) * (p2.X - p1.X) + (p2.Y - p1.Y) * (p2.Y - p1.Y);
        }

        public int[] Find(int[,] adjacencyMatrix, int sourceIndex, int targetIndex) {
            Dijkstra(adjacencyMatrix, sourceIndex, out int[] distance, out int[] parent);

            var list = new List<int>();

            BuildPath(sourceIndex, targetIndex, distance, parent, list);

            if (list.Count < 2) {
                return Array.Empty<int>();
            }
            else {
                return list.ToArray();
            }
        }

        private void Relax(int u, int v, int weight, int[] distance, int[] parent) {
            if (distance[u] != int.MaxValue && distance[v] > distance[u] + weight) {
                distance[v] = distance[u] + weight;
                parent[v] = u;
            }
        }

        private void Dijkstra(int[,] adjacencyMatrix, int source, out int[] distance, out int[] parent) {
            int totalVertices = adjacencyMatrix.GetLength(0);

            distance = Enumerable.Repeat(int.MaxValue, totalVertices).ToArray();
            distance[source] = 0;

            parent = Enumerable.Repeat(-1, totalVertices).ToArray();

            var vertexIndexByDistance = new SortedList<int, int>(new DuplicateKeyComparer<int>());

            for (int i = 0; i < totalVertices; i++) {
                vertexIndexByDistance.Add(distance[i], i); // priority = distance, item = vertex index
            }

            while (vertexIndexByDistance.Count > 0) {
                var u = vertexIndexByDistance.ElementAt(0).Value;
                vertexIndexByDistance.RemoveAt(0);

                for (int v = 0; v < adjacencyMatrix.GetLength(0); v++) {
                    if (adjacencyMatrix[u, v] > 0) {
                        Relax(u, v, adjacencyMatrix[u, v], distance, parent);

                        int pos = vertexIndexByDistance.IndexOfValue(v);
                        if (pos != -1) {
                            vertexIndexByDistance.RemoveAt(pos);
                            vertexIndexByDistance.Add(distance[v], v);
                        }
                    }
                }
            }
        }

        private void BuildPath(int u, int v, int[] distance, int[] parent, List<int> path) {
            if (v < 0 || u < 0) {
                return;
            }
            if (v != u) {
                BuildPath(u, parent[v], distance, parent, path);
                path.Add(v);
            }
            else {
                path.Add(v);
            }
        }
    }
}
